import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TimetableDashboardExamsComponent } from './timetable-dashboard-exams.component';

describe('TimetableDashboardExamsComponent', () => {
  let component: TimetableDashboardExamsComponent;
  let fixture: ComponentFixture<TimetableDashboardExamsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimetableDashboardExamsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimetableDashboardExamsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
