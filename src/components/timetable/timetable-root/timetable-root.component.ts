import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { TableConfiguration } from '@universis/ngx-tables';
import { cloneDeep } from 'lodash';
import { ModalService, TemplatePipe, AppEventService, ErrorService } from '@universis/common';
import { TimetablePrintComponent } from '../timetable-print/timetable-print.component';

@Component({
  selector: 'app-timetable-root',
  templateUrl: './timetable-root.component.html'
})
export class TimetableRootComponent implements OnInit, OnDestroy {

  public model: any;
  private paramSubscription: Subscription;
  public actions: any[];
  public config: any;
  public allowedActions: any[];
  public edit: any;

  constructor(
    private _router: Router,
    private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private http: HttpClient,
    private _template: TemplatePipe,
    private _eventService: AppEventService,
    private _modalService: ModalService,
    private _errorService: ErrorService
  ) { }

  ngOnInit() {
    this.paramSubscription = this._activatedRoute.params.subscribe(async ({ timetable }) => {
      this.model = await this._context.model('TimetableEvents')
        .where('id').equal(timetable)
        .select('id,organizer/name as department,organizer/id as organizer,eventStatus,startDate,endDate,name')
        .getItem();

      try {
        this.http.get(`assets/lists/TimetableEvents/all.json`).pipe(
          map((result) => {
            return result as TableConfiguration;
          })
        ).subscribe((config) => {
          // @ts-ignore
          this.config = cloneDeep(config);
          if (this.config.columns && this.model) {
            // get actions from config file
            this.actions = this.config.columns.filter(x => {
              return x.actions;
            })
              // map actions
              .map(x => x.actions)
              // get list items
              .reduce((a, b) => b, 0);

            this.edit = this.actions.find(x => x.role === 'edit');
            this.actions = this.actions.filter(x => x.role === 'action' || x.role === 'method');

            for (const action of [...this.actions, ...this.edit]) {
              if (action.formatOptions && action.formatOptions.commands) {
                action.formatOptions.commands = action.formatOptions.commands.map(c => typeof c === 'string' ? this._template.transform(c, this.model): c);
                const urlTree = this._router.createUrlTree(action.formatOptions.commands, {
                  ...action.formatOptions.navigationExtras,
                  relativeTo: this._activatedRoute.firstChild
                });
                action.href = urlTree.toString()
              } else if (action.href) {
                action.href = this._template.transform(action.href, this.model);
              } else if (action.invoke) {
                action.click = () => {
                  if (typeof this[action.invoke] === 'function') { this[action.invoke](); }
                };
              }
            }

            // if there are report templates associated with the timetable model, add print action
            this._context.model('ReportTemplates')
              .where('reportCategory/appliesTo').equal('TimetableEvent')
              .getItems()
              .then((reportTemplates) => {
                if (reportTemplates && reportTemplates.length) {
                  this.actions.push({
                    title: 'Events.TimetableEvents.Export',
                    click: () => this.print()
                  })
                }
              });

          }
        });
      } catch (error) {
        console.log(error)
      }

    });
  }

  async print() {
    try {
      this._modalService.openModalComponent(TimetablePrintComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          formProperties: { organizer: this.model.organizer },
          execute: new Observable((observer) => {
            // get values from modal component
            const component = this._modalService.modalRef.content as TimetablePrintComponent;
            const { studyProgram, reportTemplate } = component.formComponent.form.formio.data;

            // emit the values necessary for printing the report
            this._eventService.change.next({
              reportTemplate,
              model: 'TimetableEvent',
              action: 'print',
              params: {
                studyProgram,
                "ID": this.model.id,
              }
            });

            observer.next();
          })
        }
      });
    } catch (err) {
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

}
