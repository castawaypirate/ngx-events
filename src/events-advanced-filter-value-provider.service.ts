import { Injectable } from '@angular/core';
import { TemplatePipe } from '@universis/common';
import { AdvancedFilterValueProvider } from '@universis/ngx-tables';

@Injectable({
    providedIn: 'root'
})
export class EventsFilterValueProvider extends AdvancedFilterValueProvider {

    public values = { currentDepartment: null };
    
    constructor() {
        super()
    }

    setValues(values) {
        if (typeof values === 'object') {
            Object.assign(this.values, values);
        }
    }

}
